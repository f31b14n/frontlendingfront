import { createApplication } from '@/api/application'

const NAMESPACE = 'store/application'

export default {
  namespaced: true,
  state: {
    identifier: null,
    name: null,
    requestAmount: null,
    uuid: null,
    status: null,
    errored: null
  },
  mutations: {
    setIdentifier (state, value) {
      value = parseInt(value)
      if (typeof value !== 'number') {
        console.error(`identifier is not number in ${NAMESPACE}.setIdentifier`)
      } else {
        state.identifier = value
      }
    },
    setName (state, value) {
      if (typeof value !== 'string') {
        console.error(`name is not string in ${NAMESPACE}.setName`)
      } else {
        state.name = value
      }
    },
    setRequestAmount (state, value) {
      value = parseInt(value)
      if (typeof value !== 'number') {
        console.error(`requestAmount is not number in ${NAMESPACE}.setRequestAmount`)
      } else {
        state.requestAmount = value
      }
    }
  },
  actions: {
    newApplication ({ state }) {
      state.uuid = null
      state.status = null

      return new Promise((resolve, reject) => {
        createApplication({
          identifier: state.identifier,
          name: state.name,
          request_amount: state.requestAmount
        }).then(response => {
          state.uuid = response.data.uuid
          state.status = response.data.status

          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}
