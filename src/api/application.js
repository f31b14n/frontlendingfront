import apiClient from './apiClient'

const createApplication = (data) => apiClient.post('application', data)

export {
  createApplication
}
